---
<p align="center"> <img src="https://user-images.githubusercontent.com/49681382/101802266-48204a00-3b20-11eb-85ec-08c123fca79e.png" height="270" width="277" /> </p>

<h2 align="center"> ⚠️ OmicLearn project is now moved to <a href="https://github.com/MannLabs/OmicLearn" target="_blank">MannLabs/OmicLearn</a>.</h2>

<p align="center">OmicLearn project is no longer maintained at OmicEra, it is now moved to <a href="https://github.com/MannLabs/OmicLearn" target="_blank">MannLabs/OmicLearn</a>.</p>

---

## OmicLearn (obsolete version)
Transparent exploration of machine learning for biomarker discovery from proteomics and omics data.

![OmicLearn Tests](https://github.com/OmicEra/OmicLearn/workflows/OmicLearn%20Tests/badge.svg)
![OmicLearn Python Badges](https://img.shields.io/badge/Tested_with_Python-3.8-blue)
![OmicLearn Version](https://img.shields.io/badge/Release-v1.1.3-orange)
![OmicLearn Release](https://img.shields.io/badge/Release%20Date-May%202022-green)
![OmicLearn License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)

## Manuscript
📰 <a href="https://doi.org/10.1101/2021.03.05.434053" target="_blank">Open-access article: **Transparent exploration of machine learning for biomarker discovery from proteomics and omics data**</a>

> **Citation:** <br>
> Transparent exploration of machine learning for biomarker discovery from proteomics and omics data <br>
> Furkan M Torun, Sebastian Virreira Winter, Sophia Doll, Felix M Riese, Artem Vorobyev, Johannes B Müller-Reif, Philipp E Geyer, Maximilian T Strauss <br>
> bioRxiv 2021.03.05.434053; doi: https://doi.org/10.1101/2021.03.05.434053


## Online Access

🟢  <a href="https://omiclearn.com" target="_blank"> OmicLearn.com </a>

## Local Installation & Running

> More information about `Installation & Running` is available on our **[documentation page](https://omiclearn.readthedocs.io/en/latest/)**.

- It is strongly recommended to install OmicLearn in its own environment using [Anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html).

  1. Redirect to the folder of choice and clone the repository: `git clone https://github.com/OmicEra/OmicLearn`
  2. Install the required packages with `conda env create --file environment.yml`
  3. Activate the environment with  `conda activate omic_learn`

- After a successful installation, type the following command to run OmicLearn:

  `streamlit run omic_learn.py --browser.gatherUsageStats False`

  > `Running with Docker` option is also available. Please, check the installation instructions on **[the documentation page](https://omiclearn.readthedocs.io/en/latest/)**.

 - After starting the streamlit server, the OmicLearn page should be automatically opened in your browser (Default link: [`http://localhost:8501`](http://localhost:8501)

## Getting Started with OmicLearn

The following image displays the main steps of OmicLearn:

![OmicLearn Workflow](https://user-images.githubusercontent.com/49681382/91734594-cb421380-ebb3-11ea-91fa-8acc8826ae7b.png)

Detailed instructions on how to get started with OmicLearn can be found **[here.](https://omiclearn.readthedocs.io/en/latest/)**

On this page, you can click on the titles listed in the *Table of Contents*, which contain instructions for each section.

## Contributing

As OmicLearn project is no longer maintaned at OmicEra and it is moved to [MannLabs/OmicLearn](https://github.com/MannLabs/OmicLearn), please refer to https://github.com/MannLabs/OmicLearn.

All contributions are welcome. 👍

